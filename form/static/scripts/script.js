$(document).ready(function(){
  var tomorrow = new Date()
  tomorrow.setDate(tomorrow.getDate()+1)
  $('.datepicker').datepicker({
    'format':'yyyy-mm-dd',
    minDate: tomorrow
  });
});

$(document).ready(function(){
  $('.modal').modal();
});

$(document).ready(function(){
  $('.datepicker').change(function(e) {
    var elem = e.target;
    var today = new Date();
    var millisecondsperday = 1000 * 60 * 60 * 24;
    var end_date = new Date(elem.value);
    end_date.setDate(end_date.getDate()+1)
    var form = $(this).closest('form');
    var millis_between = end_date.getTime() - today.getTime();
    var days = Math.ceil(millis_between / millisecondsperday);
    var cost = form.find('input[name=vehicle_price]').val()
    var num_days = form.find('.number_of_days')

    if (days > 0) {
      num_days.removeClass('red-text');
      if (days > 1) {
        num_days.html('Your cost from ' + today.getMonth() + '/' +
          today.getDate() + '/' + today.getFullYear() + ' through ' +
          end_date.getMonth() + '/' + end_date.getDate() + '/' + 
          end_date.getFullYear() + ' (' + days + ' days) is: <em class="green-text">$' + cost*days + '</em>')
      }
      else {
        num_days.html('Your cost from ' + today.getMonth() + '/' +
          today.getDate() + '/' + today.getFullYear() + ' through ' +
          end_date.getMonth() + '/' + end_date.getDate() + '/' + 
          end_date.getFullYear() + ' (' + days + ' day) is: <em class="green-text">$' + cost*days + '</em>')
      }
    }
    else {
      num_days.addClass('red-text')
      num_days.html('Please select a valid return date!')
    }
  })
})

$(document).ready(function() {
  $('.return_mileage').keyup(function(e) {
    var elem = e.target;
    var form = $(this).closest('form')
    var price = form.find('input[name=price]').val()
    var startd = new Date(form.find('input[name=start_date]').val())
    var returnd = new Date(form.find('input[name=return_date]').val())
    var start_mileage = form.find('input[name=start_mileage]').val()
    var mileage = form.find('input[name=return_mileage]').val()
    var millisecondsperday = 1000 * 60 * 60 * 24;
    var millis_between = returnd.getTime() - startd.getTime();
    var days = Math.ceil(millis_between / millisecondsperday);

    if(start_mileage - mileage < 0) {
      $("div.total").html((price*days) + (.32*mileage))
    }
  })
})

