from django.shortcuts import get_object_or_404, get_list_or_404, render, redirect
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.template import loader
from django.urls import reverse
import datetime as dt

from .models import CarModel
from .models import Vehicle

def index(request):
    return HttpResponse("Hello, world!")

def models(request):
    models = get_list_or_404(CarModel)
    template = loader.get_template('carModels/index.html')
    context = {
        'models': models,
    }
    return HttpResponse(template.render(context, request))

def choices(request, model_id):
    vehicles = get_list_or_404(Vehicle, model_id=model_id)
    model = get_object_or_404(CarModel, id=model_id)
    today = dt.date.today()
    return render(request, 'vehicles/index.html', {'model': model, 'vehicles': vehicles, 'today': today})

def detail(request, model_id, vehicle_id):
    vehicle = get_object_or_404(Vehicle, id=vehicle_id)
    model = get_object_or_404(CarModel, id=model_id)
    return render(request, 'vehicle/index.html', {'model': model, 'vehicle': vehicle})

def rentals(request):
    vehicles = get_list_or_404(Vehicle, renter=request.user)
    model = get_list_or_404(CarModel)
    today = dt.date.today()
    return render(request, 'rentals/index.html', {'vehicles': vehicles, 'today': today, 'model': model})

def rent(request, model_id, vehicle_id):
    # vehicle = get_object_or_404(Vehicle, id=vehicle_id)
    # model = get_object_or_404(CarModel, id=model_id)
    try:
        vehicle = Vehicle.objects.get(id=request.POST.get('vehicle_choice'))
        start_date = request.POST.get('start_date')
        return_date = request.POST.get('return_date')
    except (KeyError, Vehicle.DoesNotExist):
        return render(request, 'vehicle/index.html', {
            'model': model,
            'vehicle': vehicle,
            'error_message': 'It appears this vehicle is no longer available.'
        })
    else:
        vehicle.available = not vehicle.available
        vehicle.start_date = dt.date.today()
        vehicle.return_date = return_date
        vehicle.renter = request.user
        vehicle.save()
        return HttpResponseRedirect(reverse('cars:models'))
    return

def v_return(request, vehicle_id):
    try:
        vehicle = Vehicle.objects.get(id=request.POST.get('vehicle_choice'))
        start_mileage = vehicle.mileage
        mileage = request.POST.get('return_mileage')
    except (KeyError, Vehicle.DoesNotExist):
        return render(request, 'rentals/index.html', {
            'vehicle': vehicle,
            'error_message': 'It appears this vehicle was already returned.'
        })
    else:
        vehicle.available = not vehicle.available
        vehicle.mileage = int(mileage)
        vehicle.start_date = None
        vehicle.return_date = None
        vehicle.renter = None
        vehicle.save()
        return HttpResponseRedirect(reverse('cars:rentals'))
    return

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('models')
    else:
        form = UserCreationForm()
    return render(request, 'signup/index.html', {'form': form})

def model_redirect(request):
    return HttpResponseRedirect(reverse('cars:models'))

