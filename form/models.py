from django.db import models
from django.conf import settings

class CarModel(models.Model):
    model_type = models.CharField(max_length=200)
    rental_price = models.DecimalField(max_digits=4, default=19.95, decimal_places=2)

    def __str__(self):
        return self.model_type

class Vehicle(models.Model):
    model = models.ForeignKey(CarModel, on_delete=models.CASCADE)
    make = models.CharField(max_length=200, default=None)
    start_date = models.DateField('starting date', default=None, null=True)
    return_date = models.DateField('return date', default=None, null=True)
    mileage = models.IntegerField(default=0)
    renter = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True)
    available = models.BooleanField(default=True)

    def __str__(self):
        return self.make
