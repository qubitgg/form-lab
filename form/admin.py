from django.contrib import admin
from .models import CarModel, Vehicle

admin.site.register(CarModel)
admin.site.register(Vehicle)
