from django import template

register = template.Library()

@register.filter
def multiply(v1, v2):
    out = v1 * int(v2)
    return out