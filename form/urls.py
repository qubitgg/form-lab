from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

app_name = 'cars'
urlpatterns = [
    path('', views.index, name='index'),
    path('signup/', views.signup, name='signup'),
    path('login/', auth_views.login, {'template_name': 'login/index.html'}, name='login'),
    path('logout/', auth_views.logout, {'template_name': 'login/index.html'}, name='logout'),
    path('models/', views.models, name='models'),
    path('models/<int:model_id>/', views.choices, name='vehicles'),
    path('models/<int:model_id>/vehicle/<int:vehicle_id>/', views.detail, name='detail'),
    path('models/<int:model_id>/vehicle/<int:vehicle_id>/rent/', views.rent, name='rent'),
    path('rentals/return/<int:vehicle_id>/', views.v_return, name='return'),
    path('accounts/profile/', views.model_redirect),
    path('rentals/', views.rentals, name='rentals'),
]